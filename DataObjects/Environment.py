class Environment:

    def __init__(self):
        self._base_url = None
        self._token = None

    # Base URL

    @property
    def base_url(self):
        return self._base_url

    @base_url.setter
    def base_url(self, base_url):
        self._base_url = base_url

    @base_url.deleter
    def base_url(self):
        del self._base_url

    # Token

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, token):
        self._token = token

    @token.deleter
    def token(self):
        del self._token
