class Client:

    def __init__(self):
        self._first_name = None
        self._last_name = None
        self._phone = None
        self._id = None

    # First Name

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        self._first_name = first_name

    @first_name.deleter
    def first_name(self):
        del self._first_name

    # Last Name

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        self._last_name = last_name

    @last_name.deleter
    def last_name(self):
        del self._last_name

    # Phone

    @property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, phone):
        self._phone = phone

    @phone.deleter
    def phone(self):
        del self._phone

    # Id

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @id.deleter
    def id(self):
        del self._id
