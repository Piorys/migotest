import base64
import json
import os


class Utilities:

    @staticmethod
    def encode_credentials(username, password):
        """
        Encode username and password for further usage with api requests
        :param username:
        :param password:
        :return: base64 encoded string
        """
        credentials = username + ":" + password
        return base64.b64encode(credentials.encode('ascii')).decode('ascii')

    @staticmethod
    def decode_json(filename):
        """
        Decodes json from file within data/ directory to be used as dictionary
        :param filename:
        :return: dictionary object to be used further
        """
        filepath = os.getcwd() + f"/data/{filename}.json"
        f = open(filepath)
        data = json.load(f)
        return data
