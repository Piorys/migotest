import requests
from api.Utilities import *


class ClientRequests:

    @staticmethod
    def get_token(base_url, username, password):
        """
        Get api key for further usage with api requests
        :param base_url:
        :param username:
        :param password:
        :return: api key
        """
        credentials = Utilities.encode_credentials(username, password)
        payload = {}
        headers = {
            'Authorization': f'Basic {credentials}'
        }
        url = base_url + "token"

        return requests.request("POST", url, headers=headers, data=payload)

    """
    api calls for existing Clients
    """

    @staticmethod
    def get_client(base_url, token, client_id):
        """
        Get client details

        :param base_url:
        :param token:
        :param client_id:
        :return: response object
        """

        payload = {}
        headers = {
            'X-api-KEY': f'{token}'
        }
        url = base_url + "client/" + client_id

        return requests.request("GET", url, headers=headers, data=payload)

    @staticmethod
    def update_client(base_url, token, client_id, client_data):
        """
        Updates given client by id

        :param base_url:
        :param token
        :param client_id:
        :param client_data:
        :return: response object
        """
        payload = client_data
        headers = {
            'X-api-KEY': f'{token}',
            'Content-Type': 'application/json'
        }
        url = base_url + "client/" + client_id

        return requests.request("PUT", url, headers=headers, data=payload)

    @staticmethod
    def delete_client(base_url, token, client_id):
        """
        Deletes given client by id

        :param base_url:
        :param token
        :param client_id:
        :return: response object
        """
        payload = {}
        headers = {
            'X-api-KEY': f'{token}'
        }
        url = base_url + "client/" + client_id

        return requests.request("DELETE", url, headers=headers, data=payload)

    @staticmethod
    def get_clients(base_url, token):
        """
        Get client details

        :param base_url:
        :param token
        :return: response object
        """
        payload = {}
        headers = {
            'X-api-KEY': f'{token}'
        }
        url = base_url + "clients"

        return requests.request("GET", url, headers=headers, data=payload)

    @staticmethod
    def add_client(base_url, token, client_data):
        """
        Adds new client

        :param base_url:
        :param token
        :param client_data:
        :return: response object
        """
        payload = client_data
        headers = {
            'X-API-KEY': f'{token}',
            'Content-Type': 'application/json'
        }
        url = base_url + "client"

        return requests.request("POST", url, headers=headers, data=payload)
