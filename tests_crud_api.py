import pytest
from faker import Faker

from api.ClientRequests import *

from DataObjects.Client import *
from DataObjects.Environment import *
from api.Utilities import Utilities


class TestsClient:
    fake = Faker()
    client = Client()
    environment = Environment()

    @pytest.fixture(autouse=True)
    def set_token(self):
        credentials = Utilities.decode_json("Credentials")

        self.environment.base_url = Utilities.decode_json("Environment")['interview']
        self.environment.token = ClientRequests.get_token(self.environment.base_url,
                                                          credentials['interview']['username'],
                                                          credentials['interview']['password']).json()['key']

    @pytest.fixture(autouse=True)
    def set_client_data(self):
        self.client.first_name = self.fake.first_name()
        self.client.last_name = self.fake.last_name()
        self.client.phone = self.fake.phone_number()

    @pytest.fixture()
    def create_temp_client(self):
        # Creates a new client for testing purposes
        self.temp_client = Client()

        self.temp_client.first_name = self.fake.first_name()
        self.temp_client.last_name = self.fake.last_name()
        self.temp_client.phone = self.fake.phone_number()

        client_data = json.dumps({
            "firstName": f"{self.temp_client.first_name}",
            "lastName": f"{self.temp_client.last_name}",
            "phone": f"{self.temp_client.phone}"
        })
        response = ClientRequests.add_client(
            self.environment.base_url,
            self.environment.token,
            client_data
        )
        assert response.status_code == 200
        assert response.json()['firstName'] == self.temp_client.first_name
        assert response.json()['lastName'] == self.temp_client.last_name
        assert response.json()['phone'] == self.temp_client.phone

        self.temp_client.id = response.json()['id']

    @pytest.fixture()
    def teardown_temp_client(self):
        yield
        response = ClientRequests.delete_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id
        )
        assert response.status_code == 200

        # Clear Data Object
        del self.temp_client.first_name
        del self.temp_client.last_name
        del self.temp_client.phone
        del self.temp_client.id

    def test_create_client_positive(self):
        # Format client data payload
        client_data = json.dumps({
            "firstName": f"{self.client.first_name}",
            "lastName": f"{self.client.last_name}",
            "phone": f"{self.client.phone}"
        })
        response = ClientRequests.add_client(
            self.environment.base_url,
            self.environment.token,
            client_data
        )
        assert response.status_code == 200
        assert response.json()['firstName'] == self.client.first_name
        assert response.json()['lastName'] == self.client.last_name
        assert response.json()['phone'] == self.client.phone

    def test_get_client_positive(self, create_temp_client, teardown_temp_client):
        response = ClientRequests.get_client(self.environment.base_url, self.environment.token, self.temp_client.id)

        assert response.status_code == 200
        assert response.json()['firstName'] == self.temp_client.first_name
        assert response.json()['lastName'] == self.temp_client.last_name
        assert response.json()['phone'] == self.temp_client.phone

    def test_update_client_positive(self, create_temp_client, teardown_temp_client):
        # Prepare data for modifying client

        name = self.fake.first_name()
        last_name = self.fake.last_name()
        phone = self.fake.phone_number()

        client_data = json.dumps({
            "firstName": f"{name}",
            "lastName": f"{last_name}",
            "phone": f"{phone}"
        })

        response = ClientRequests.update_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id,
            client_data,
        )

        assert response.status_code == 200

        get_client = ClientRequests.get_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id
        )

        # This assertion is failing, possible bug on side of server. firstName is not changing.
        assert get_client.json()['firstName'] == name

        assert get_client.json()['lastName'] == last_name
        assert get_client.json()['phone'] == phone

    def test_delete_client_positive(self, create_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['removeClient']['clientRemoved']

        delete_client_response = ClientRequests.delete_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id
        )

        assert delete_client_response.status_code == 200
        assert delete_client_response.json()['message'] == expected_message
        get_client_response = ClientRequests.get_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id
        )

        assert get_client_response.status_code == 404

    def test_get_clients_positive(self, create_temp_client, teardown_temp_client):
        get_clients_response = ClientRequests.get_clients(
            self.environment.base_url,
            self.environment.token
        )

        assert get_clients_response.status_code == 200

        client_to_search = json.dumps({
            "firstName": f"{self.temp_client.first_name}",
            "id": f"{self.temp_client.id}",
            "lastName": f"{self.temp_client.last_name}",
            "phone": f"{self.temp_client.phone}"
        })
        # This assertion will also fail, as get_clients will always return same clients
        assert client_to_search in get_clients_response.json()['clients']

    def test_get_client_negative_not_found(self):
        expected_message = Utilities.decode_json("ServerMessages")['general']['notFound']
        response_get = ClientRequests.get_client(
            self.environment.base_url,
            self.environment.token,
            "123123"
        )

        assert response_get.status_code == 404
        assert response_get.json()['message'] == expected_message

    def test_get_client_negative_invalid_token(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidToken']

        response_get = ClientRequests.get_client(
            self.environment.base_url,
            "123123123",
            self.temp_client.id
        )

        assert response_get.status_code == 403
        assert response_get.json()['message'] == expected_message

    def test_update_client_negative_not_found(self):
        expected_message = Utilities.decode_json("ServerMessages")['general']['notFound']

        client_data = json.dumps({
            "firstName": f"{self.fake.first_name()}",
            "lastName": f"{self.fake.last_name()}",
            "phone": f"{self.fake.phone_number()}"
        })

        response_get = ClientRequests.update_client(
            self.environment.base_url,
            self.environment.token,
            "123123",
            client_data
        )

        assert response_get.status_code == 404
        assert response_get.json()['message'] == expected_message

    def test_update_client_negative_invalid_token(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidToken']

        client_data = json.dumps({
            "firstName": f"{self.temp_client.first_name}",
            "lastName": f"{self.temp_client.last_name}",
            "phone": f"{self.temp_client.phone}",
        })

        response_get = ClientRequests.update_client(
            self.environment.base_url,
            "123123123",
            self.temp_client.id,
            client_data
        )

        assert response_get.status_code == 403
        assert response_get.json()['message'] == expected_message

    def test_update_client_negative_missing_first_name(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingFirstName']

        client_data = json.dumps({
            "lastName": f"{self.temp_client.last_name}",
            "phone": f"{self.temp_client.phone}",
        })

        response_get = ClientRequests.update_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id,
            client_data
        )

        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_update_client_negative_missing_last_name(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingLastName']

        client_data = json.dumps({
            "firstName": f"{self.temp_client.first_name}",
            "phone": f"{self.temp_client.phone}",
        })

        response_get = ClientRequests.update_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id,
            client_data
        )

        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_update_client_negative_missing_phone(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingPhone']

        client_data = json.dumps({
            "firstName": f"{self.temp_client.first_name}",
            "lastName": f"{self.temp_client.last_name}"
        })

        response_get = ClientRequests.update_client(
            self.environment.base_url,
            self.environment.token,
            self.temp_client.id,
            client_data
        )

        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_create_client_negative_invalid_token(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidToken']

        client_data = json.dumps({
            "firstName": f"{self.fake.name()}",
            "lastName": f"{self.fake.last_name()}",
            "phone": f"{self.fake.phone_number()}",
        })

        response_get = ClientRequests.add_client(
            self.environment.base_url,
            "123123123",
            client_data
        )

        assert response_get.status_code == 403
        assert response_get.json()['message'] == expected_message

    def test_create_client_negative_missing_first_name(self):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingFirstName']

        client_data = json.dumps({
            "lastName": f"{self.fake.last_name()}",
            "phone": f"{self.fake.phone_number()}",
        })

        response_get = ClientRequests.add_client(
            self.environment.base_url,
            self.environment.token,
            client_data
        )

        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_create_client_negative_missing_last_name(self):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingLastName']

        client_data = json.dumps({
            "firstName": f"{self.fake.name()}",
            "phone": f"{self.fake.phone_number()}",
        })

        response_get = ClientRequests.add_client(
            self.environment.base_url,
            self.environment.token,
            client_data
        )

        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_create_client_negative_missing_phone(self):
        expected_message = Utilities.decode_json("ServerMessages")['create_update_client']['missingPhone']

        client_data = json.dumps({
            "firstName": f"{self.fake.name()}",
            "lastName": f"{self.fake.last_name()}"
        })

        response_get = ClientRequests.add_client(
            self.environment.base_url,
            self.environment.token,
            client_data
        )
        assert response_get.status_code == 400
        assert response_get.json()['message'] == expected_message

    def test_delete_client_negative_not_found(self):
        expected_message = Utilities.decode_json("ServerMessages")['general']['notFound']

        delete_client_response = ClientRequests.delete_client(
            self.environment.base_url,
            self.environment.token,
            "123123123"
        )

        assert delete_client_response.json()['message'] == expected_message
        assert delete_client_response.status_code == 404

    def test_delete_client_negative_invalid_token(self, create_temp_client, teardown_temp_client):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidToken']

        delete_client_response = ClientRequests.delete_client(
            self.environment.base_url,
            "123123123",
            self.temp_client.id
        )

        assert delete_client_response.json()['message'] == expected_message
        assert delete_client_response.status_code == 403

    def get_clients_negative_invalid_token(self):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidToken']

        get_clients_response = ClientRequests.get_clients(
            self.environment.base_url,
            "123123123"
        )

        assert get_clients_response.json()['message'] == expected_message
        assert get_clients_response.status_code == 403

    def create_token_negative_wrong_username(self):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidCredentials']
        credentials = Utilities.decode_json("Credentials")

        create_token_response = ClientRequests.get_token(
            self.environment.base_url,
            "123123123",
            credentials['interview']['password']
        )

        assert create_token_response.status_code == 400
        assert create_token_response.json()['message'] == expected_message

    def create_token_negative_wrong_password(self):
        expected_message = Utilities.decode_json("ServerMessages")['token']['invalidCredentials']
        credentials = Utilities.decode_json("Credentials")

        create_token_response = ClientRequests.get_token(
            self.environment.base_url,
            credentials['interview']['username'],
            "123123123"
        )

        assert create_token_response.status_code == 400
        assert create_token_response.json()['message'] == expected_message
