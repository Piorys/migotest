# CRUD API TESTS

A set of test created to test crud functionalities of given API

## Requirements

- Python 3
- PIP

## Installation

Install all dependencies using following command:

``
$ pip install -r requirements.txt
``

## Execution

Use following command:

``
§ pytest tests_crud_api.py
``

## Known issues

- 3 test cases are failing, possible bug on side of API
